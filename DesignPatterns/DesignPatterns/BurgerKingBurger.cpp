#include "BurgerKingBurger.h"

BurgerKingBurger::BurgerKingBurger(IIngredientsFactory* factory)
	: m_bun{ factory->AddBun() },
	m_pattie{ factory->AddPattie() },
	m_onions{ factory->AddOnions() }
{

}

void BurgerKingBurger::CreateBurger()
{
	m_bun->Prepare();
	m_pattie->Prepare();
	
	if (m_onions == nullptr)
	{
		std::cout << "No Onions";
	}
	else
	{
		m_onions->Prepare();
	}
}
