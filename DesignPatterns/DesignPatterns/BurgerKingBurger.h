#pragma once
#include "IBurger.h"
#include "IIngredientsFactory.h"

class BurgerKingBurger : public IBurger
{
public:
	BurgerKingBurger(IIngredientsFactory* factory);

	void CreateBurger() override;

private:
	IBun* m_bun;
	IPattie* m_pattie;
	IOnions* m_onions;
};

