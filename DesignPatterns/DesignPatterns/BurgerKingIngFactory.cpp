#include "BurgerKingIngFactory.h"

IBun* BurgerKingIngFactory::AddBun()
{
	return new SesameSeedBun();
}

IPattie* BurgerKingIngFactory::AddPattie()
{
	return new FlameBrolledPattie();
}

IOnions* BurgerKingIngFactory::AddOnions()
{
	return nullptr;
}
