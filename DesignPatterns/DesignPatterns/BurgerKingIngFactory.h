#include "IIngredientsFactory.h"
#include "SesameSeedBun.h"
#include "FlameBrolledPattie.h"
#include "SweetPickle.h"

class BurgerKingIngFactory : public IIngredientsFactory
{
	IBun* AddBun() override;
	IPattie* AddPattie() override;
	IOnions* AddOnions() override;
};

