#pragma once
#include <iostream>

class IBun
{
public:
	virtual void Prepare() const noexcept = 0;
};

