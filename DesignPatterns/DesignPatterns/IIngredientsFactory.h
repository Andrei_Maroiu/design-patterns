#pragma once
#include <iostream>
#include "IPattie.h"
#include "IBun.h"
#include "IOnions.h"

class IIngredientsFactory
{
public:
	virtual IPattie* AddPattie() = 0;
	virtual IBun* AddBun() = 0;
	virtual IOnions* AddOnions() = 0;
};

