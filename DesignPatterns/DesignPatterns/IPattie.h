#pragma once
#include <iostream>

class IPattie
{
public:
	virtual void Prepare() const noexcept = 0 ;
};

