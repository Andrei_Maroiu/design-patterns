#pragma once
class IPickle
{
	virtual void Prepare() const noexcept = 0;
};
