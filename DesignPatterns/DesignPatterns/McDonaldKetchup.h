#pragma once
#include<iostream>
#include"IKetchup.h"
class McDonaldKetchup :public IKetchup
{
	void Prepare() const noexcept override {
		std::cout << "Preparing McDonaldKetchup" << std::endl;
	}
};
