#include "McDonaldingFactory.h"

IBun* McDonaldingFactory::AddBun()
{
	return new NormalBun();
}

IPattie* McDonaldingFactory::AddPattie()
{
	return new GrilledPattie();
}

IOnions* McDonaldingFactory::AddOnions()
{
	return new WhiteOnions();
}
