#include "IIngredientsFactory.h"
#include "NormalBun.h"
#include "WhiteOnions.h"
#include "GrilledPattie.h"

class McDonaldingFactory : public IIngredientsFactory
{
public:
	IBun* AddBun() override;
	IPattie* AddPattie() override;
	IOnions* AddOnions() override;
};

