#include "McDonaldsBurger.h"

McDonaldsBurger::McDonaldsBurger(IIngredientsFactory* factory)
	: m_bun{factory->AddBun()},
	m_pattie{factory->AddPattie()},
	m_onions{factory->AddOnions()}
{
}

void McDonaldsBurger::CreateBurger()
{
	m_bun->Prepare();
	m_pattie->Prepare();
	m_onions->Prepare();
}
