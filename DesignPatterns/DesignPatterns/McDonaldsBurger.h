#pragma once
#include "IBurger.h"
#include "IIngredientsFactory.h"

class McDonaldsBurger : public IBurger
{
public:
	McDonaldsBurger(IIngredientsFactory* factory);
	void CreateBurger() override;

private:
	IBun* m_bun;
	IPattie* m_pattie;
	IOnions* m_onions;
};

